/**
 * Module dependencies.
 */

import Koa from 'koa';
import app from '../';
import config from 'config';
import logger from '../core/utils/logger';

/**
 * Start server.
 */

const startServer = (appInstance: Koa) => {
  const port = config.get('server.port');

  appInstance.listen(port);

  logger.info(`Listening on ${port}`);
};

/**
 * Start server.
 */

app().then(startServer); // tslint:disable-line
