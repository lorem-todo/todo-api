/**
 * Module dependencies.
 */

import { BadRequestError } from 'easy-http-errors';
import Ajv from 'ajv';
import Koa from 'koa';

/**
 * Ajv instance.
 */

const ajv = new Ajv({
  allErrors: true
});

/**
 * Export `testContext` validator.
 */

export function testContext(ctx: Koa.Context, schema: any) {
  const validate = ajv.compile(schema);
  const valid = validate(ctx);

  if (validate.errors) {
    throw new BadRequestError('bad_request', {
      data: validate.errors.map(({ keyword, message, params }) => ({
        keyword,
        message,
        params
      }))
    });
  }

  return valid;
}

/**
 * Create.
 */

const create = {
  body: {
    additionalProperties: false,
    properties: {
      age: { type: 'number' },
      birthday: { type: 'string' },
      email: {
        format: 'email',
        type: 'string'
      },
      name: { type: 'string' }
    },
    required: ['age', 'birthday', 'email', 'name'],
    type: 'object'
  }
};

/**
 * Update.
 */

const update = {
  body: {
    additionalProperties: false,
    properties: {
      age: { type: 'number' },
      birthday: { type: 'string' },
      email: {
        format: 'email',
        type: 'string'
      },
      name: { type: 'string' }
    },
    type: 'object'
  },
  params: {
    additionalProperties: false,
    properties: {
      id: { type: 'string' }
    },
    type: 'object'
  }
};

/**
 * Delete.
 */

const deleteSchemas = {
  params: {
    additionalProperties: false,
    properties: {
      id: { type: 'string' }
    },
    type: 'object'
  }
};

/**
 * Get.
 */

const get = {
  params: {
    additionalProperties: false,
    properties: {
      id: { type: 'string' }
    },
    type: 'object'
  }
};

/**
 * Paginate.
 */

const paginate = {
  query: {
    additionalProperties: false,
    properties: {
      'page[number]': {
        pattern: '[0-9]+',
        type: 'string'
      },
      'page[size]': {
        pattern: '[0-9]+',
        type: 'string'
      }
    },
    required: ['page[number]', 'page[size]'],
    type: 'object'
  }
};

/**
 * Export `schemas`.
 */

export const schemas = {
  create,
  delete: deleteSchemas,
  get,
  paginate,
  update
};
