/**
 * Module dependencies.
 */

import { schemas, testContext } from './validations';
import Koa from 'koa';
import qs from 'qs';
import todoDao from 'managers/todo-dao';

/**
 * `TodoController` class.
 */

class TodoController {

  /**
   * Create.
   */

  public static create(ctx: Koa.Context) {
    testContext(ctx.request.body, schemas.create.body);

    ctx.body = todoDao.create(ctx.request.body);
    ctx.status = 200;
  }

  /**
   * Update.
   */

  public static update(ctx: Koa.Context) {
    testContext(ctx.request.body, schemas.update.body);
    testContext(ctx.params, schemas.update.params);

    ctx.body = todoDao.update(ctx.params.id, ctx.request.body);
    ctx.status = 200;
  }

  /**
   * Delete.
   */

  public static delete(ctx: Koa.Context) {
    testContext(ctx.params, schemas.delete.params);

    todoDao.delete(ctx.params.id);

    ctx.body = null;
    ctx.status = 204;
  }

  /**
   * Get.
   */

  public static get(ctx: Koa.Context) {
    testContext(ctx.params, schemas.get.params);

    ctx.body = todoDao.get(ctx.params.id);
    ctx.status = 200;
  }

  /**
   * Get.
   */

  public static paginate(ctx: Koa.Context) {
    testContext(ctx.query, schemas.paginate.query);
    const { page } = qs.parse(ctx.query);
    const { number, size } = page as any;
    const data = todoDao.list();
    const chunk = data.slice(size * Math.min(0, number - 1), size);

    ctx.body = {
      data: chunk,
      total: data.length
    };
    ctx.status = 200;
  }

}

/**
 * Export `TodoController` class.
 */

export default TodoController;
