/**
 * Module dependencies.
 */

import { InternalServerError } from 'easy-http-errors';
import { random } from 'lodash';
import Koa from 'koa';
import KoaRouter from 'koa-router';
import StandardHttpError from 'standard-http-error';
import TodoController from './controllers/todo-controller';
import koaBody from 'koa-body';

/**
 * Error middleware.
 */

const errorMiddleware = async (ctx: any, next: () => Promise<any>) => {
  try {
    await next();
  } catch (error) {
    if (error instanceof StandardHttpError && error?.data && error?.code) {
      ctx.body = error?.data;
      ctx.status = error?.code;

      return;
    }

    throw error;
  }
};

/**
 * Random errors middleware.
 */

const randomErrorsMiddleware = async (_ctx: any, next: () => Promise<any>) => {
  const shoudlReturnISE = random(0, 2) === 0;
  const delaytimer = random(0, 1500);

  if (shoudlReturnISE) {
    throw new InternalServerError({
      data: {
        code: 'im_a_crasy_server',
        message: 'This is a purposeful random error.'
      }
    });
  }

  await next();

  await new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, delaytimer);
  });
};

/**
 * App.
 */

const app = async (): Promise<any> => {
  const app = new Koa(); // tslint:disable-line

  app.use(koaBody({ multipart: true }));

  const router = new KoaRouter();

  router.use(errorMiddleware);
  router.use(randomErrorsMiddleware);

  router.post('/todo', TodoController.create);
  router.patch('/todo/:id', TodoController.update);
  router.delete('/todo/:id', TodoController.delete);
  router.get('/todo/:id', TodoController.get);
  router.get('/todo', TodoController.paginate);

  app.use(router.routes());

  const result = await Promise.resolve(app);

  return result;
};

/**
 * Export `app`.
 */

export default app;
