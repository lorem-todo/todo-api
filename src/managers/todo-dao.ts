/**
 * Module dependencies.
 */

import { NotFoundError } from 'easy-http-errors';
import { findIndex, pick } from 'lodash';
import { v4 as uuid } from 'uuid';

/**
 * `Todo` type.
 */

type Todo = {
  id: string | undefined,
  name: string,
  email: string,
  age: string,
  birthday: string | null
}

/**
 * `TodoDao` class.
 */

class TodoDao {

  /**
   * Data.
   */

  private data: Todo[];

  /**
   * Constructor.
   */

  constructor() {
    this.data = [];
  }

  /**
   * Get item.
   */

  getItem(id: string) {
    const index = findIndex(this.data || [], (todo: Todo) => todo?.id === id);

    if (index === -1) {
      throw new NotFoundError('not_found');
    }

    return {
      data: this.data[index],
      index
    };
  }

  /**
   * Create.
   */

  public create(todo: Todo): Todo {
    const index = this.data.push({
      ...todo,
      id: uuid()
    });

    return this.data[index - 1];
  }

  /**
   * Update.
   */

  public update(id: string, todo: any): Todo {
    const { data, index } = this.getItem(id);
    const payload = pick(todo, ['id', 'name', 'email', 'age', 'birthday']);

    this.data[index] = {
      ...data,
      ...payload
    };

    return this.data[index];
  }

  /**
   * Delete.
   */

  public delete(id: string): void {
    const { index } = this.getItem(id);

    this.data.splice(index, 1);
  }

  /**
   * Get.
   */

  public get(id: string): Todo {
    const { index } = this.getItem(id);

    return this.data[index];
  }

  /**
   * List.
   */

  public list(): Todo[] {
    return this.data;
  }

}

/**
 * Export `TodoDao` instance.
 */

export default new TodoDao();
