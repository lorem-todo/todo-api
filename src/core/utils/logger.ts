/**
 * Module dependencies.
 */

import bunyan from 'debugnyan';

/**
 * Export default `logger` util.
 */

export default bunyan('mock:api');
