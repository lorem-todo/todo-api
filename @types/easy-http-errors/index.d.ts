
/**
 * `@seegno/easy-http-errors` types.
 */

declare module 'easy-http-errors' {

  class BaseError {
    constructor(message?: any, options?: any)
  }

  export class NotFoundError extends BaseError {}

  export class BadRequestError extends BaseError {}

  export class InternalServerError extends BaseError {}

}
