
/**
 * `debugnyan` types.
 */

declare module 'debugnyan' {
  function bunyan(name: string): {
    error: (key: string) => void,
    info: (key: string) => void
  };

  export = bunyan;
}
