# Endpoints

### POST /todo

Create a new `todo`.

#### Request Parameters

Property    | Type      | Required
----------- | --------- | --------
age         | `Number`  | `true`
birthday    | `String`  | `true`
email       | `String`  | `true`
name        | `object`  | `true`

**Request body**

```json
{
	"age": 1,
	"birthday": "01-01-01",
	"email": "foo@bar.com",
	"name": "foobar"
}
```

#### Response

**200 success**

```json
{
  "age": 1,
  "birthday": "01-01-01",
  "email": "foo@bar.com",
  "id": "00a39bd9-1920-418a-b50d-0aaaf1a81dea",
  "name": "foobar"
}
```

### PATCH /todo/:id

Edit a `todo` by id.

#### Request Parameters

Property    | Type      | Required
----------- | --------- | --------
age         | `Number`  | `false`
birthday    | `String`  | `false`
email       | `String`  | `false`
name        | `object`  | `false`

**Request body**

```json
{
  "age": 2
}
```

#### Response

**200 success**

```json
{
  "age": 2,
  "birthday": "01-01-01",
  "email": "foo@bar.com",
  "id": "00a39bd9-1920-418a-b50d-0aaaf1a81dea",
  "name": "foobar"
}
```

### DELETE /todo/:id

Delete a `todo` by id.

#### Response

**204 success**

### GET /todo/:id

Get a `todo` by id.

#### Response

**200 success**

```json
{
  "age": 2,
  "birthday": "01-01-01",
  "email": "foo@bar.com",
  "id": "00a39bd9-1920-418a-b50d-0aaaf1a81dea",
  "name": "foobar"
}
```

### GET /todo

Get paginated `todo` list.

#### Required query options

Name         | Description                                 | Example
------------ | ------------------------------------------- | -----------------
page[size]   | Defines the number of results per page.     | `page[size]=10`
page[number] | Defines the number of the page to retrieve. | `page[number]=1`

#### Response

**200 success**

```json
{
  "data": [
    {
      "age": 12,
      "birthday": "21-08-83",
      "email": "foo@bar.com",
      "name": "OK",
      "id": "768de225-efee-40b4-9974-ccba49a86385"
    },
    {
      "age": 12,
      "birthday": "21-08-83",
      "email": "foo@bar.com",
      "name": "OK",
      "id": "82bd4c9b-b546-4543-b68a-1b9ce609b08c"
    }
  ],
  "total": 12
}
```
