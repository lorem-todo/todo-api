FROM node:10.15.0-alpine AS base

USER node
RUN mkdir /home/node/server
WORKDIR /home/node/server

FROM base AS node-dependencies

USER root
RUN apk --no-cache --virtual build-dependencies add g++ gcc git make python openssh-client yarn
USER node

COPY --chown=node:node . ./
RUN NODE_ENV=production yarn bundle

FROM base AS build-dist

COPY --chown=node:node --from=node-dependencies /home/node/server /home/node/server

RUN chmod +x /home/node/server/docker-entrypoint.sh

ENTRYPOINT ["/home/node/server/docker-entrypoint.sh"]

CMD node dist/bin/index.js