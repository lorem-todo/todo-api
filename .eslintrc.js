module.exports =  {
  parser:  '@typescript-eslint/parser',  // Specifies the ESLint parser
  plugins: ['@typescript-eslint'],
  root: true,
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      "modules": true
    },
    includes: [
      'src/**/*.ts'
    ],
  },
  overrides: [{
    files: [ '*.ts' ],
    rules: {
      'no-unused-vars': 'off',
      'capitalized-comments': [
        'error',
        'always',
        {
            'ignorePattern': 'tslint|pragma|ignored',
            'ignoreInlineComments': true
        }
      ],
      'no-inline-comments': 'off'
    }
  }],
  extends:  [
    'seegno'
  ],
  settings: {
   react: {
     version: '99999.99999'
   }
 }
};
