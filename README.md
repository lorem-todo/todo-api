# Todo API

This is a small API with CRUD endpoints to manage a list of TODOs in memory. You can consult the API documentation [here](docs/endpoints.md).

## Challenge

The challenge is to make an interface in React and/or React Native to manage the data in this API. You should take into account possible errors and show them properly in the developed interface.

## Requirements to run the server

- Docker engine

## Get Started

Create a file in the folder where you will start the project called `docker-compose.yml` and place it inside:

```
version: '3.3'

services:
  server:
    image: registry.gitlab.com/lorem-todo/todo-api:latest
    restart: on-failure
    environment:
      - DEBUG=mock*
    ports:
      - 8000:8000

```

Next, you should run the following command in your terminal:

```sh
docker-compose up
```

And that's it... the server is now running. O endpoint de acesso é `http://localhost:8000`.

You can use invoke this endpoint to test API: `http://localhost:8000/todo?page[number]=1&page[size]=10`.

Good luck.